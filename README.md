# Micronaut filter issue

## Use case

Given more then one Controller, Client and Filter.

Filters applied based on the Client id, 
but whenever you called any of the client, 
then all the Filters invoked in a wierd way.

## Instruction

```bash 
./gradlew run
```

Call any client and you can see the issue in the console log.

Available endpoints:

- http://localhost:8080/client1
- http://localhost:8080/client2
- http://localhost:8080/client3

__OR__

```bash 
./gradlew test
```

It will call the 1st client, you can see the isse in the console log.

###### NOTE

_In this case we don't care about the HttpClientException (Because of the client URL doesn't exist)_