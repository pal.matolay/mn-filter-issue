package example

import io.micronaut.http.HttpResponse
import io.micronaut.http.MutableHttpRequest
import io.micronaut.http.annotation.Filter
import io.micronaut.http.filter.ClientFilterChain
import io.micronaut.http.filter.HttpClientFilter
import org.reactivestreams.Publisher

@Filter(serviceId = ["client1"], patterns = ["/**"])
class Client1Filter : HttpClientFilter {

    override fun doFilter(request: MutableHttpRequest<*>, chain: ClientFilterChain): Publisher<out HttpResponse<*>> {
        request.header("x-api-key", "CLIENT_1_SECRET")
        printRequestData(request)
        return chain.proceed(request)
    }
}

@Filter(serviceId = ["client2"], patterns = ["/**"])
class Client2Filter : HttpClientFilter {

    override fun doFilter(request: MutableHttpRequest<*>, chain: ClientFilterChain): Publisher<out HttpResponse<*>> {
        request.header("x-api-key", "CLIENT_2_SECRET")
        printRequestData(request)
        return chain.proceed(request)
    }
}


@Filter(serviceId = ["client3"], patterns = ["/**"])
class Client3Filter : HttpClientFilter {

    override fun doFilter(request: MutableHttpRequest<*>, chain: ClientFilterChain): Publisher<out HttpResponse<*>> {
        request.header("x-api-key", "CLIENT_3_SECRET")
        printRequestData(request)
        return chain.proceed(request)
    }
}

private fun printRequestData(request: MutableHttpRequest<*>) {
    println("\nreq. data\nURI:\n${request.uri}")
    println("HEADER(api-key):\n"+request.headers["x-api-key"])
}