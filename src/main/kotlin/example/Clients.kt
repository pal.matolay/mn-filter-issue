/*
 * Copyright 2018 original authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package example

import io.micronaut.http.HttpResponse
import io.micronaut.http.MutableHttpRequest
import io.micronaut.http.annotation.Filter
import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.filter.ClientFilterChain
import io.micronaut.http.filter.HttpClientFilter
import io.reactivex.Single
import org.reactivestreams.Publisher

import javax.validation.constraints.NotBlank

@Client(id = "client1")
interface Client1 {

    @Get("/client1")
    fun call(): Single<String>
}


@Client(id = "client2")
interface Client2 {

    @Get("/client2")
    fun call(): Single<String>
}

@Client(id = "client3")
interface Client3 {

    @Get("/client3")
    fun call(): Single<String>
}