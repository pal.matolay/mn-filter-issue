package example

import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class DummyTest {

    @Inject
    lateinit var client1: Client1

    @Test
    fun client1() {
        print("Calling ${this.client1::class.simpleName}")
        client1.call().blockingGet()
    }
}
